﻿import sys
import struct
import re
import binascii

#Author Lena A-Sabino

Lable_Table = {}
if(len(sys.argv)!=3): 
    print("need assembler file and output file")
    exit(1)

lines = open(sys.argv[1], "r").read().split('\n')
outputFile=sys.argv[2]
lineNumber=0
address = 0
tokens = []

#converts instructions in assembler file to binary 

def alpha(opc,param):   
    for s in t['param'][1:-1]:              
        param = ' '.join(format(ord(x), 'b') for x in s)
        temp=opc+'00000'+'{0:08b}'.format(int(param,2))
        write_file(temp)
    return True

def cmp(opc, param):
    if param[1:-1]=='':
        temp=opc+'0000100000000'
        write_file(temp)
    else:
        alpha(opc, param)
    return True
def brae(opc, param):
    param= Lable_Table[param]
    temp=opc + '{0:013b}'.format(param)
    write_file(temp)
    return True

def brane(opc, param):
    if not param in Lable_Table:
        print("No such a lable ", param , " on line ", lineNumber)
        return False
    else: param= Lable_Table[param]
    temp=opc + '{0:013b}'.format(param)
    write_file(temp)
    return True

def draw(opc, paramD, l, paramM):
    paramD=paramD[1:-1]
    if paramD== '': 
        erase( opcode['erase'],'{0:04b}'.format(int(paramM))+l)
        return True
    paramD = ' '.join(format(ord(x), 'b') for x in paramD)
    if int(paramM) > 0 :  
        temp=opc+'{0:04b}'.format(int(paramM))+str(l)
        temp+='{0:08b}'.format(int(paramD,2))
    else:temp=opc+'00000'+'{0:08b}'.format(int(paramD,2))
    write_file(temp)
    return True

def move(opc,l,param):
    temp=opc+'{0:04b}'.format(int(param))+str(l)+'00000000'
    write_file(temp)
    return True

def stop(opc, param):
    temp=opc+'0000'+str(param)+'00000000'
    write_file(temp)
    return True

def erase(opc,param):
    temp=opc+param+'00000000'
    write_file(temp)
    return True

opcode = {'alpha': "000", 'cmp' : "001", 'brae' :"011", 'brane': "010", 'draw': "100", 'halt': "110", 'fail': "110", 'move': "101", 'erase': "111" }
instructions={'alpha' : alpha, 'cmp' : cmp, 'brae' : brae, 'brane' : brane, 'draw' : draw, 'move' : move, 'stop' : stop, 'erase' : erase, 'fail': stop, 'halt':stop}

def escapeAll(string):    
    string=re.sub(r'[\\t\\s\\d\\f\\r]','',string)
    return string
def get_quots(line):
    inst
    for i in line:
        if i == '\'':
            inst
    
def getTokens(token):  
    dic = {}
    dic['instraction'] = token[0]
    if 1 < len(token):
        #string=token[1][1:-1]
        string = escapeAll(token[1])
        dic['param'] = string
    else:
        if token[0] == 'halt': dic['param'] = 1
        else: dic['param'] = 0
    tokens.append(dic)
    return True
    

try:
    assert(len(sys.argv) != 1), ("need asm file")
except:
    exit(1)

for index, line in enumerate(lines):
    lineNumber+=1
    line = line.strip()
    if len(line) > 0 :        
        
        if line.startswith("alpha"): 
            line=line.split(" ") 
            m=line[1][1:-1]  
            if '\'' in m or '\"' in m: 
                print("Invalid token on line ", lineNumber) 
                exit(0)  
            address+=len(m)
            getTokens(line)

        elif (line.startswith("!") and (lines[index + 1] is not None) and (not lines[index + 1].startswith("!"))):    
            Lable_Table[line] = address   
            
        elif(line.startswith("bra ")):
             address+=2   
             getTokens(line.split(" "))
        
        elif(line.startswith('draw') and lines[index + 1] is not None):
            if(lines[index + 1].startswith("\tright") or lines[index + 1].startswith("\tleft")):
                getTokens(line.split(" "))
        elif (not line.startswith("#")):
            getTokens(line.split(" "))
            address+=1

def write_file(t):
    file = open(outputFile, "ab+")
    e = struct.pack('>H',  int(t,2))
    file.write(e)   
    file.close
    
for index, t in enumerate(tokens):
        if t['instraction'] == "draw":           
            if ((index+1 < len(tokens))and(tokens[index+1]['instraction']=="left" or tokens[index+1]['instraction']=="right")):  
                l='0'
                if tokens[index+1]['instraction']=="left":
                    l='1'  
                good=instructions[t['instraction']](opcode[t['instraction']],t['param'],l, tokens[index+1]['param'])
            else:
                 good=instructions[t['instraction']](opcode[t['instraction']], t['param'])
        elif t['instraction'] == "right" or t['instraction'] == "left":
            if (index-1 >=0 and  not tokens[index-1]['instraction']=="draw" ):  
                l=0
                if t['instraction'] == "left":l=1
                good=instructions['move'](opcode['move'], l,t['param'])

        elif t['instraction']=="bra":
            good=instructions['brane'](opcode['brane'], t['param'])
            good=instructions['brae'](opcode['brae'], t['param'])
        else:
             
             good=instructions[t['instraction']](opcode[t['instraction']], t['param'])
        if(good==False):break
exit(1)


