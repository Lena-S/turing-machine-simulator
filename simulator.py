﻿import struct
import sys

#Author Lena A-Sabino

alphabet=[]
equals=False

head=0
tape=[]

INSTRACTION=0
instructionName=''
RAM=[]
PC=0
 
moves=0
draws=0
cycles=0
stageName=''
instractions=0
if (len(sys.argv)!=3):
    print("need assembler file and tape file")
    exit(1)

f = open(sys.argv[1], "rb")
tapes= open(sys.argv[2], "r").read().split('\n')

def error(message):
    #global tape, stageName
    print("---------------------------------------------------------------------------------")
    print("Fail")
    print("Error during stage ' ", stageName, "'  error message -'", message, "' " )
    print("Tape ", tape)
    print("Moves = ", moves, " Cycles = ", cycles, "  Instractions ", instractions)

def append(param):
    global tape, head
    for i in range(len(tape),len(tape)+param+1):
        tape.insert(i,'')
    head= len(tape)-1

def prepend(param):
    global tape, head
    for i in range(len(tape),param):
        tape.insert(0,'')
    head=0
def alpha(param):
    param=param[5:]
    char=chr(int(param,2))
    if not char in alphabet: alphabet.append(char) 
    return True

def cmp(param):
    global equals, tape, head
    empty=param[4]
    char =param[5:]

    if(int(empty) == 1): 
        if head>=len(tape) or head < 0:         
            equals=True
            return True
        if '' == tape[head]: equals=True
        else: equals=False

    else:
        
        if head>=len(tape) or head < 0:
            equals=False
        elif not tape[head] in alphabet:     
            
            print("Error character ", tape[head], " not in alphabet ", alphabet) 
            return False
        else:
            if chr(int(char,2)) == tape[head]: equals=True
            else: equals=False
    return True

def brane(param):
    global eguals, f, PC
    if equals == False:
        PC=int(param,2)
    return True
        
def brae(param):
    global f, equals, PC
    if equals == True:
        PC=int(param,2)
    return True

def draw(param):    
    global draws, tape
    char=chr(int(param[-8:],2))
    moveHead=int(param[:4],2)

    if head >= len(tape): append(head-len(tape))
    elif head <=0 : prepend(len(tape)-head)

    tape[head]=char
    draws+=1
    if  moveHead>0: move(param)
    return True
   
def move(param):
    global head, moves
    count=int(param[:4])
    l=int(param[4])
   # print("Left ", l)
    if l==0: head += count
    else: head -= count
    moves+=1
    return True

def stop(param): 
    global head
    h=int(param[4])   
    if h==1: print(" Halt ")    
    else: print(" Fail ")
    print("Move = ", moves," Draws = ", draws ,", Cycles = ", cycles, ",  Instractions ", instractions)   
    print("       ") 
    for i in tape:
        sys.stdout.write(i)  
    print("")
   
    while(head):
        sys.stdout.write('_')
        head-=1
    print("^")
    return False
   
def erase(param):     
    l=int(param[4])
    if head >= len(tape): append(head-len(tape))
    elif head <=0 : prepend(len(tape)-head)
    tape.remove(tape[head])
    if l==1: move(param)
    return True

def decode():
    global INSTRACTION, instructionName, stageName
    stageName=" decode "
    INSTRACTION='{0:016b}'.format(INSTRACTION[0])

    if ( not INSTRACTION[:3] in  opCode): 
        error("Unkown  opcode "+  INSTRACTION[:3])
        return False

    instructionName=INSTRACTION[:3]    
    return True

def execute():
    global INSTRACTION, stageName
    stageName=" execute "
    t=opCode[instructionName](INSTRACTION[3:])
    return t

def loadRam(f): 
    byte = f.read(2)
    while len(byte):  
        RAM.append(struct.unpack('>H', byte)) 
        byte = f.read(2)  

def fetch():
    global INSTRACTION, PC, RAM, instractions, stageName
    stageName=" fetch "
    if len(RAM)<=PC: 
        error(" no more instructions")
        return False
    elif(PC < 0):
        error(" no more instructions")
        return False
    INSTRACTION=RAM[PC]        
    PC +=1    
    instractions+=1
    return True

opCode={
     "000": alpha,
     "001": cmp,
     "010": brane,
     "011": brae,
     "100": draw,
     "101": move,
     "110": stop,
     "111": erase
    }

loadRam(f)
def simulator():
    global cycles
    t=True
    while(t):
        t=fetch()
        if(t==False): break
        t=decode()
        if(t==False): break
        t=execute()
        cycles+=1
    
for t in tapes:
    PC=0
    cycles=0
    head=0
    instractions=0
    tape=list(t)
    print("-------------------------------------------------------------------------------")
    print("Tape before ", tape)
    simulator()    
print("RAM size ", len(RAM))       
exit(1)