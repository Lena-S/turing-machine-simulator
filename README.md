# Universal Turing Machine Simulator#

TurAsm.py -  accepts assembly code suitable for describing the actions of arbitrary Turing machines and converts it to our binary format.

Simulator.py - executes a binary on a given file of “tapes”.

Project Description can be viewed in  TuringmachineSimulator file.

Task description can be viewed in Task_description file.

##To run Simulator:##

Task 1.  

Draw a Turing machine that accepts tape with the head pointing to the beginning of a string from the alphabet {a,b,c}.  Your machine should replace the original string with another string that has all the runs of ‘a’s, ‘b’s, and ‘c’s replaced with one ‘a’, ‘b’, or ‘c’, respectively, and reset the head to the beginning of the new string.  

Example: 
Iinput : abaabbbaaaabbbbbaaaaaa; Output: abababa  

* run 'TurAsm.py' with '/asmLetters.txt' and 'output/file/name' file as parameters  - it will produce binary file with 'output file name'

* run 'Simulator.py' with 'binary/file/name' and 'tapesletters.txt' files as parameters.